# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-06-04 17:09+0800\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: asaloader\device.py:15
msgid "Default, auto detect device type."
msgstr ""

#: asaloader\cli_arg.py:20
msgid "Program to ASA-series board."
msgstr ""

#: asaloader\cli_arg.py:30
msgid "Set the language code. e.g. `zh_TW`, `en_US`"
msgstr ""

#: asaloader\cli_arg.py:40
msgid "Program code to board."
msgstr ""

#: asaloader\cli_arg.py:49
msgid "List all available devices."
msgstr ""

#: asaloader\cli_arg.py:57
msgid "List all available serial ports."
msgstr ""

#: asaloader\cli_arg.py:69
msgid "The name or number of the device type to be programmed. "
msgstr ""

#: asaloader\cli_arg.py:70
msgid "Can see available device type by subcommand print-device-list."
msgstr ""

#: asaloader\cli_arg.py:81
msgid "The serial port which program burn the device."
msgstr ""

#: asaloader\cli_arg.py:92
msgid "Set binary file which program to flash."
msgstr ""

#: asaloader\cli_arg.py:103
msgid "Set binary file which program to eeprom."
msgstr ""

#: asaloader\cli_arg.py:114
msgid "Enter the application after programing."
msgstr ""

#: asaloader\cli_arg.py:125
msgid ""
"Set delay time from programing completion to executing application.(in ms)"
msgstr ""

#: asaloader\cli_arg.py:150
msgid "Error: Parameter --device is illegal."
msgstr ""

#: asaloader\cli_arg.py:158
msgid ""
"Error: No flash or eeprom needs to be burned, please use '-f ', '-e ' to "
"specify the file."
msgstr ""

#: asaloader\cli_arg.py:165
#, python-brace-format
msgid "Error: Cannot find flash binary file {0}."
msgstr ""

#: asaloader\cli_arg.py:170
#, python-brace-format
msgid "Error: The flash binary file {0} is not ihex formatted."
msgstr ""

#: asaloader\cli_arg.py:177
#, python-brace-format
msgid "Error: Cannot find eeprom binary file {0}."
msgstr ""

#: asaloader\cli_arg.py:182
#, python-brace-format
msgid "Error: The eeprom binary file {0} is not ihex formatted."
msgstr ""

#: asaloader\cli_arg.py:188
#, python-brace-format
msgid "Error: Cannot find serial port {0}."
msgstr ""

#: asaloader\cli_arg.py:189
msgid "The available serial ports are as follows:"
msgstr ""

#: asaloader\business.py:21
msgid "Available device list:\n"
msgstr ""

#: asaloader\business.py:22
msgid "    device name   \t num \t note\n"
msgstr ""

#: asaloader\business.py:48
msgid "ERROR: com port has been opened by another application."
msgstr ""

#: asaloader\business.py:78 asaloader\business.py:110 asaloader\business.py:115
msgid "ERROR: Can't communicate with the device."
msgstr ""

#: asaloader\business.py:79
msgid "       Please check the comport and the device."
msgstr ""

#: asaloader\business.py:82
msgid "ERROR: Device is not match."
msgstr ""

#: asaloader\business.py:83
msgid "       Assigned device is '{0:s}'"
msgstr ""

#: asaloader\business.py:85
msgid "       Detected device is '{0:s}'"
msgstr ""

#: asaloader\business.py:89
msgid "Device is '{0:s}'"
msgstr ""

#: asaloader\business.py:91
#, python-brace-format
msgid "Flash  hex size is {0:0.2f} KB ({1} bytes)"
msgstr ""

#: asaloader\business.py:93
#, python-brace-format
msgid "EEPROM hex size is {0} bytes."
msgstr ""

#: asaloader\business.py:94
#, python-brace-format
msgid "Estimated time  is {0:0.2f} s."
msgstr ""

#: asaloader\business.py:97
#, python-format
msgid "Elapsed Time: %(seconds)0.2fs"
msgstr ""

#: asaloader\business.py:111 asaloader\business.py:116
msgid "Please check the comport is correct."
msgstr ""
