# -*- coding: utf-8 -*-

import conftest

from asaloader import alp

def test_alp_decoder():
    predict = {
        'command': alp.Command.CHK_PROTOCOL,
        'data': b'test'
    }

    pac_raw = b'\xfc\xfc\xfc\xfa\x01\x00\x04test\xc0'
    
    d = alp.Decoder()
    for ch in pac_raw:
        d.step(ch)
        if d.isDone():
            real = d.getPacket()
    
    assert(predict == real)

def test_alp_encoder():
    predict = b'\xfc\xfc\xfc\xfa\x01\x00\x04test\xc0'
    
    pac = {
        'command': alp.Command.CHK_PROTOCOL,
        'data': b'test'
    }

    real = alp.encode(pac['command'], pac['data'])
    
    assert(predict == real)

